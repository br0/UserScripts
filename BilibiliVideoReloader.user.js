// ==UserScript==
// @name Bilibili Video Reloader
// @version 0.0.4
// @namespace Violentmonkey Scripts
// @description 增加辣鸡网络环境下b站的可用性
// @downloadURL https://gitlab.com/br0/UserScripts/raw/master/BilibiliVideoReloader.user.js
// @match *://www.bilibili.com/video/av*
// @grant none
// ==/UserScript==
$(document).ready(function(){
  var app_name = 'Bilibili Video Reloader';

  function rafAsync() {
    return new Promise(resolve => {
      requestAnimationFrame(resolve); //faster than set time out
    });
  }

  function checkElement(selector) {
    if (document.querySelector(selector) === null) {
      return rafAsync().then(() => checkElement(selector));
    } else {
      return Promise.resolve(true);
    }
  }

  function parseGetParameters() {
    var query = {}, queryString = window.location.search;
    var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
    for (var i = 0; i < pairs.length; i++) {
      var pair = pairs[i].split('=');
      query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
    return query;
  }

  function getPartFromUrl() {
    var matches = window.location.pathname.match(/\/video\/av\d+\/index_(\d+)\.html$/i);
    return matches ? (matches[1] || 1) : 1;
  }

  function getVideoAidFromUrl() {
    var matches = window.location.pathname.match(/\/video\/av(\d+).+/i);
    return matches ? matches[1] : null;
  }

  function download_datauri(dataurl, filename) {
    var a = document.createElement("a");
    a.href = dataurl;
    a.setAttribute("download", filename);
    var b = document.createEvent("MouseEvents");
    b.initEvent("click", false, true);
    a.dispatchEvent(b);
    return false;
  }

  function reload_video(){
    console.info(`${app_name}: Try to reload video`);
    var player_time = window.player.getCurrentTime(),
        params = parseGetParameters(),
        param_time = params['t'] || player_time,
        param_part = params['p'] || 1,
        url_part = getPartFromUrl(),
        video_aid = window.player.option('aid') || getVideoAidFromUrl(),
        part, time;
    part = Math.max(param_part, url_part);
    time = parseInt(Math.max(param_time, player_time));

    if (!video_aid) {
      console.error(`${app_name}: Cannot identify video aid, time: ${time}, part: ${part}`);
    } else {
      window.location = `https://www.bilibili.com/video/av${video_aid}?t=${time}&p=${part}`;
    }
    return false;
  }

  function take_screenshot(){
    var w = player.getWidth(),
        h = player.getHeight(),
        canvas = document.createElement('canvas'),
        ctx = canvas.getContext('2d'),
        aid = player.option('aid'),
        time = player.getCurrentTime().toString(),
        filename = `bilibili_av${aid}_t_${time}.png`;
    canvas.width = w;
    canvas.height = h;
    ctx.drawImage(document.getElementById('bilibiliPlayer').querySelector('video'), 0, 0, w, h);
    download_datauri(canvas.toDataURL('image/png'), filename);
    delete canvas;
    return false;
  }

  var reload_button = $(`
<div name="reload_video" class="bilibili-player-video-btn bilibili-player-video-btn-reload">
  <i class="bilibili-player-iconfont" title="重新加载">R</i>
</div>`);
  var capture_button = $(`
<div name="reload_video" class="bilibili-player-video-btn bilibili-player-video-btn-capture" title="Capture">
  <i class="bilibili-player-iconfont" title="Capture">C</i>
</div>`);

  checkElement('.bilibili-player-video-time').then((element) => {
    reload_button.on('click', reload_video).insertBefore('.bilibili-player-video-btn-widescreen');
    capture_button.on('click', take_screenshot).insertBefore('.bilibili-player-video-btn-reload');
    console.log(`${app_name}: button injected`);
  });

})